#!/usr/bin/env bash

export GRAFANA_HOST=${GRAFANA_HOST:-'http://grafana:3000'}
if [[ -z ${GRAFANA_TOKEN} ]]; then
    # No token, try with default user/pass for local development instance
    export GRAFANA_USERNAME=admin
    export GRAFANA_PASSWORD=admin
fi

grafana-dashboard-builder -p grafana-driver.yaml --exporter grafana
grafana-dashboard-builder -p grafana-marge.yaml --exporter grafana
