#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


import logging
from contextlib import contextmanager
from dataclasses import dataclass
from datetime import datetime, timezone
from os import environ
from typing import Any, Dict, Tuple, Optional

from dateutil.relativedelta import relativedelta
from influxdb_client import InfluxDBClient, Point, WriteApi


class InfluxdbTokenManager:
    bucket_tokens: dict[str, Optional[str]] = {
        "mesa-perf-v2": environ.get("INFLUX_TOKEN"),
        "marge-stats": environ.get("STATS_INFLUX_TOKEN")
    }
    fallback_token = "influx-testing-token"

    @staticmethod
    def get_token_from_bucket_name(bucket_name) -> str:
        token = InfluxdbTokenManager.bucket_tokens.get(bucket_name)
        if not token:
            logging.debug(
                "Could not find token for bucket "
                f"{bucket_name}, falling back to "
                f"{InfluxdbTokenManager.fallback_token}."
            )
            return InfluxdbTokenManager.fallback_token

        return token

@dataclass
class InfluxdbSource:
    """Wrapper for influxdb_client library.
    Each instance is fixed to a bucket.
    It also manages which token to use based on the bucket name.

    Args:
        bucket (str): Bucket name
        write_since_weeks (int): How many weeks this source should look in the
                                    past to begin data extraction.
        org (str): Organization name
        url (str): URL location of the influxdb
    """

    __bucket: str = environ.get("INFLUX_BUCKET", "mesa-perf-v2")
    __write_since_weeks: int = 4
    __org: str = environ.get("INFLUX_ORG", "freedesktop")
    __url: str = environ.get("INFLUX_URL", "http://influxdb:8086")

    def __post_init__(self):
        self.__token = InfluxdbTokenManager.get_token_from_bucket_name(self.__bucket)
        self.__client = InfluxDBClient(
            url=self.__url, token=self.__token, org=self.__org
        )

    def write(self, point: Point):
        self.__writer.write(bucket=self.__bucket, org=self.__org, record=point)

    @staticmethod
    def create_point(name: str) -> Point:
        return Point(name)

    def get_start_date(self):
        """Calculate the time to begin data extraction based on
        write_since_weeks.

        Returns:
            datetime: the date to begin fetching Gitlab results.
        """
        return datetime.now(timezone.utc) - relativedelta(
            weeks=self.__write_since_weeks
        )

    def get_last_write(self, measurement: str, field: str, range_start: str = "-4w"):
        """Calculate the time to begin data extraction based on the last time
        recorded in the bucket.

        Args:
            measurement (str): the name of measurement
            field (str): a field to reduce the query size
            range_start (str): Flux formatted time to be used as range start
                                argument.

        Returns:
            datetime: the date to begin fetching Gitlab results.
        """
        query = f"""from(bucket: "{self.__bucket}")
                |> range(start: {range_start})
                |> filter(fn: (r) => r._measurement == "{measurement}"
                    and r._field == "{field}"
                )
                |> group()
                |> last()
                |> keep(columns: ["_time"])"""

        logging.debug(f"Last time query:\n{query}")

        if result := self.__client.query_api().query(query, org=self.__org):
            last_write = result[0].records[0].get_time() + relativedelta(seconds=1)
        else:
            # Fallback to start date
            last_write = self.get_start_date()

        logging.debug(f"Last write result: {last_write}")
        return last_write

    @contextmanager
    def writer_instance(self) -> WriteApi:
        """Get into writer API context with properly flushing."""
        self.__writer = self.__client.write_api()
        with self.__writer as write_api:
            yield write_api
            write_api.flush()


@dataclass(frozen=True)
class InfluxdbDictWriter:
    """Helper class to simplify the data input extraction as a dict.

    ```
    tags = ("tag1", "tag2")
    fields = ("field",)
    dw = DictWriter(tags, fields, influx_source)

    input_data = {
        "tag1": 1
        "tag2": 2,
        "field": "abc",
    }

    # insert_data will deal with the complexity of creating a datapoint inside
    # InfluxDB
    dw.insert_data(("point", input_data, time_value)
    """

    __tags: Tuple
    __fields: Tuple
    __source: InfluxdbSource

    def __post_init__(self):
        # Check if tags and fields does not have values in common.
        assert set(self.__tags) & set(self.__fields) == set()

    def insert_data(
        self, measurement: str, input_data: Dict[str, Any], time_value, *args, **kwargs
    ):
        """Insert data into InfluxDB via a validated input dictionary

        Args:
            measurement (str): the name of the point measurement
            input_data (Dict[str, str]): a dict with all of point data, except timing.
            time_value (_type_): the time associated with this point
            *args, **kwargs: arguments sent to point time function
        """
        p = self.__source.create_point(measurement)

        for tag_name in self.__tags:
            p = p.tag(tag_name, input_data[tag_name])

        for field_name in self.__fields:
            p = p.field(field_name, input_data[field_name])

        p = p.time(time_value, *args, **kwargs)

        self.__source.write(p)
