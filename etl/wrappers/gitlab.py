#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import bz2
import json
import logging
import time
from dataclasses import dataclass
from datetime import datetime
from os import environ

import requests

from gitlab import Gitlab
from gitlab.base import RESTObjectList


@dataclass
class GitlabConnector:
    pipeline_wait_time_sec: int = 2
    _url: str = environ.get("GITLAB_URL", "https://gitlab.freedesktop.org")
    _project_path: str = environ.get("GITLAB_PROJECT_PATH", "mesa/mesa")
    _gitlab_token: str = environ.get("GITLAB_TOKEN", "<token unspecified>")

    def __post_init__(self):
        self.__gl = Gitlab(url=self._url, private_token=self._gitlab_token)
        self._project = self.__gl.projects.get(self._project_path)

    @property
    def project(self):
        return self._project

    @property
    def project_path(self):
        return self._project_path

    def pipeline_list(self, updated_after: datetime, **kwargs):
        list_args = {
            "as_list": False,
            "per_page": 50,
            "sort": "asc",
            "updated_after": updated_after.isoformat(),
            "username": "marge-bot",
        }

        list_args.update(kwargs)

        yield from self._project.pipelines.list(**list_args)

    def wait_for_pipeline(self, sha):
        while True:
            if pipelines := self._project.pipelines.list(sha=sha):
                try:
                    return pipelines[0]
                except AttributeError:
                    pipelines: RESTObjectList
                    return pipelines.next()
            time.sleep(self.pipeline_wait_time_sec)

    @staticmethod
    def get_results(job):
        """- `return` A Results object with the results artifacts of the specified job"""
        if job.pipeline["status"] == "running":
            logging.warning("Pipeline {} still running".format(job.pipeline["id"]))
            return None

        url = "{}/artifacts/raw/results/results.json.bz2".format(job.web_url)
        print("Requesting results from: {}".format(url))
        r = requests.get(url)

        if r.status_code != 200:
            print("Failed to download ({}):\n\t{}".format(r.status_code, url))
            return None

        return json.loads(bz2.decompress(r.content))
