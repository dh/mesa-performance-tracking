import argparse
import concurrent.futures
import gitlab
import os
import re
import smtplib
import sys
import yaml
from collections import Counter
from datetime import datetime, timedelta
from email.mime.text import MIMEText
from itertools import count
from tabulate import tabulate

GITLAB_URL = os.environ.get('GITLAB_URL', 'https://gitlab.freedesktop.org')
GITLAB_PROJECT_PATH = os.environ.get('GITLAB_PROJECT_PATH', 'mesa/mesa')
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN', '<token unspecified>')

SMTP_SERVER = os.environ.get('SMTP_SERVER', '<server unspecified>')
SMTP_SERVER_PORT = os.environ.get('SMTP_SERVER_PORT', 587)
SMTP_USER = os.environ.get('SMTP_USER', '<user unspecified>')
SMTP_PASSWD = os.environ.get('SMTP_PASSWD', '<passwd unspecified>')
SMTP_EMAIL = os.environ.get('SMTP_EMAIL', '<email unspecified>')


class ComputeObjs:
    def __init__(self):
        self.objs = []

    def run(self):
        # call all functions starting with compute_once__
        for i in dir(self):
            func = getattr(self, i)
            if i.startswith('compute_once_') and callable(func):
                func()

        # for every obj, call all functions starting with compute_obj_
        for gl_obj in self.objs:
            for i in dir(self):
                func = getattr(self, i)
                if i.startswith('compute_obj_') and callable(func):
                    func(gl_obj)


class ComputeGlObjs(ComputeObjs):
    # gl_objs could be pipelines or jobs
    def __init__(self, period=None):
        super().__init__()
        self.n_failed = 0
        self.n_success = 0
        self.period = period

    def filter_out(self, gl_obj):
        if not self.period:
            return False
        if (hasattr(gl_obj, 'finished_at') and gl_obj.finished_at
           and gl_obj.finished_at < self.period[0]):
            return True
        if (hasattr(gl_obj, 'updated_at')
           and gl_obj.updated_at < self.period[0]):
            return True
        return False

    def compute_obj_count_failed_gl_objs(self, gl_obj):
        if gl_obj.status == "success":
            self.n_success += 1
        elif gl_obj.status == "failed":
            self.n_failed += 1
        elif gl_obj.status not in ["canceled", "skipped", "manual"]:
            raise Exception("Unknown status ",  gl_obj.status,  gl_obj)

    def is_empty(self):
        return not self.n_failed and not self.n_success

    def __add__(self, other):
        if type(self) != type(other):
            raise TypeError("types don't match")
        objType = type(self)
        obj = objType()
        obj.n_failed = self.n_failed + other.n_failed
        obj.n_success = self.n_success + other.n_success
        return obj

    def __str__(self):
        return f"{type(self)}: n_failed: {self.n_failed}, n_success: {self.n_success}"  # noqa: E501


class ComputeJobsExtraInfo(ComputeObjs):
    def __init__(self, traces_config=None):
        super().__init__()
        # dictionary in the format {error name: counter}
        self.categories_counter = Counter()
        # dictionary in the format {job_id: error_reason}
        self.error_categories = {}
        self.traces_config = traces_config
        # ephemeral variable, should be released after computed
        self.trace = None
        self.job_id = None

    def __add__(self, other):
        if type(self) != type(other):
            raise TypeError("types don't match")
        objType = type(self)
        obj = objType()
        # sum the values with same keys
        obj.categories_counter = (
            self.categories_counter + other.categories_counter)
        obj.error_categories = self.error_categories | other.error_categories
        return obj

    def run(self):
        super().run()
        # Release memory
        self.trace = None

    def gitlab_request_job_extrainfo(self, project, job):
        if job.status != "failed":
            return
        # We need jobs object from the project to get traces
        pjob = project.jobs.get(job.id)
        self.trace = pjob.trace().decode('UTF-8')
        self.job_id = job.id

    def compute_once_categorize_errors(self):
        if not self.traces_config or not self.job_id:
            return

        for category in self.traces_config["categories"]:
            # if exclusive, do nothing if already classified
            if (category.get("exclusive", True) and
               self.job_id in self.error_categories):
                continue

            p = '|'.join(category["patterns"])
            match = re.findall(p, self.trace or '')
            name = category["name"]
            if match:
                # Set category only for the first match (priority order)
                if self.job_id not in self.error_categories:
                    self.error_categories[self.job_id] = name
                # Count category occurrence
                self.categories_counter.update([name])


class ComputeJobs(ComputeGlObjs):
    def __init__(self, period=None):
        super().__init__(period)
        self.pipelines = Counter()
        self.names = Counter()
        self.failed_jobs = []
        # dict with format "stage_pipeid" = job_obj
        self.stages_more30min = {}
        self.compute_extra_info = ComputeJobsExtraInfo()
        self.n_jobs_per_stage = Counter()

    def __add__(self, other):
        obj = super().__add__(other)
        # sum the values with same keys
        obj.pipelines = self.pipelines + other.pipelines
        obj.names = self.names + other.names
        obj.failed_jobs = self.failed_jobs + other.failed_jobs
        obj.n_jobs_per_stage = self.n_jobs_per_stage + other.n_jobs_per_stage
        obj.compute_extra_info = (self.compute_extra_info +
                                  other.compute_extra_info)
        # shallow copy is used because the objects are read only after filled
        obj.stages_more30min = self.stages_more30min.copy()
        obj.stages_more30min.update(other.stages_more30min)
        return obj

    def gitlab_request_jobs_in_pipeline_in_page(self, pipeline, page):
        list_args = {
            "as_list": False,
            "page": page,
            "per_page": 100,
            "include_retried": True,
            "username": 'marge-bot',
        }
        jobs = pipeline.jobs.list(**list_args)
        self.objs = [job for job in jobs if not self.filter_out(job)]
        return self.objs

    def compute_obj_count_pipelines(self, job):
        if job.status != "failed":
            return
        p_id = job.pipeline["id"]
        self.pipelines.update([p_id])

    def compute_obj_count_names(self, job):
        if job.status != "failed":
            return
        self.names.update([job.name])

    def compute_obj_list_of_failed_jobs(self, job):
        if job.status != "failed":
            return
        self.failed_jobs.append(job)

    def compute_obj_stage_duration(self, job):
        if job.status not in ["failed", "success", "canceled"]:
            return

        key = f'{job.pipeline["id"]}_{job.stage}'
        tot_duration = job.queued_duration + (job.duration or 0)

        if tot_duration < 30*60:
            return

        compare = self.stages_more30min.get(key)
        compare_time = (compare.queued_duration + (compare.duration or 0)
                        if compare else 0)

        if compare_time < tot_duration:
            self.stages_more30min[key] = job

    def compute_obj_cnt_stages(self, job):
        key = f'{job.pipeline["id"]}_{job.stage}'
        self.n_jobs_per_stage.update([key])

    def __str__(self):
        return f'{super().__str__()} | {str(self.pipelines)} | {str(self.names)}'  # noqa: E501


# Compute object to extract information per pipeline
class ComputePipelinesExtraInfo(ComputeObjs):
    def __init__(self):
        super().__init__()
        # dictionary in the format {pipeline_id: mr_title}
        self.mr_info = {}
        self.pipelines_more1h = []
        self.pipelines_more30min = []

    def __add__(self, other):
        if type(self) != type(other):
            raise TypeError("types don't match")
        objType = type(self)
        obj = objType()
        obj.mr_info = self.mr_info.copy()
        obj.mr_info.update(other.mr_info)
        obj.pipelines_more1h = self.pipelines_more1h + other.pipelines_more1h
        obj.pipelines_more30min = (self.pipelines_more30min +
                                   other.pipelines_more30min)
        return obj

    def get_mr_name_from_ref(self, project, ref):
        mr_id = ref.split("/")[2]
        mr = project.mergerequests.get(mr_id)
        return mr.title

    def gitlab_request_pipeline_extrainfo(self, project, pipeline):
        self.mr_info[pipeline.id] = self.get_mr_name_from_ref(project,
                                                              pipeline.ref)

        # fetching pipeline from the project provides more info
        pipeline = project.pipelines.get(pipeline.id)
        total_duration = pipeline.duration + pipeline.queued_duration
        if total_duration >= 60*60:
            self.pipelines_more1h.append(pipeline)
        if total_duration >= 30*60:
            self.pipelines_more30min.append(pipeline)


class ComputePipelines(ComputeGlObjs):
    def __init__(self, period=None):
        super().__init__(period)
        self.computed_jobs = ComputeJobs(period)
        self.compute_extra_info = ComputePipelinesExtraInfo()

    def __add__(self, other):
        obj = super().__add__(other)
        obj.computed_jobs = self.computed_jobs + other.computed_jobs
        obj.compute_extra_info = (self.compute_extra_info +
                                  other.compute_extra_info)
        return obj

    def __str__(self):
        return f'{super().__str__()} | {str(self.computed_jobs)}'

    def gitlab_request_pipelines_in_page(self, project, page):
        list_args = {
            "as_list": False,
            "per_page": 100,
            "page": page,
            "updated_after": self.period[0] + "T00:00:00.000Z",
            "updated_before": self.period[1] + "T23:59:59.999Z",
            "username": 'marge-bot',
            "scope": "finished",
            "source": "merge_request_event"
        }
        pipelines = project.pipelines.list(**list_args)
        self.objs = [pipeline for pipeline in pipelines
                     if not self.filter_out(pipeline)]
        return self.objs


class CiProcessor:
    def __init__(self, project, period, traces_config):
        self.project = project
        self.period = period
        self.n_pages = 4
        self.traces_config = traces_config

    # Call function in parallel and sum the results
    def process_parallel(self, ret_type, function, *argv):
        with concurrent.futures.ThreadPoolExecutor() as executor:
            results = executor.map(function, *argv)
        return sum(results, ret_type())

    def process_pages(self, ret_type, init_page, function_in_page, *argv):
        return self.process_parallel(
                        ret_type, function_in_page,
                        range(init_page, init_page + self.n_pages), *argv)

    def process_pipelines_in_page(self, page):
        compute = ComputePipelines(self.period)
        pipelines = compute.gitlab_request_pipelines_in_page(
                                            self.project, page)
        compute.run()
        compute.computed_jobs = self.process_parallel(
                                            ComputeJobs,
                                            self.process_jobs_in_pipeline,
                                            pipelines)
        compute.compute_extra_info = self.process_parallel(
                                            ComputePipelinesExtraInfo,
                                            self.process_pipeline_extra_info,
                                            pipelines)
        return compute

    def process_pipeline_extra_info(self, pipeline):
        compute = ComputePipelinesExtraInfo()
        compute.gitlab_request_pipeline_extrainfo(self.project, pipeline)
        compute.run()
        return compute

    def process_job_extra_info(self, job):
        compute = ComputeJobsExtraInfo(self.traces_config)
        compute.gitlab_request_job_extrainfo(self.project, job)
        compute.run()
        return compute

    def process_jobs_in_pipeline_in_page(self, page, pipeline):
        compute = ComputeJobs(self.period)
        jobs = compute.gitlab_request_jobs_in_pipeline_in_page(pipeline, page)
        compute.compute_extra_info = self.process_parallel(
                                            ComputeJobsExtraInfo,
                                            self.process_job_extra_info,
                                            jobs)
        compute.run()
        return compute

    def process_jobs_in_pipeline(self, pipeline):
        results = ComputeJobs()
        for pg in count(1, self.n_pages):
            results_pg = self.process_pages(
                ComputeJobs, pg, self.process_jobs_in_pipeline_in_page,
                [pipeline] * self.n_pages
            )
            results += results_pg
            if results_pg.is_empty():
                break
        return results

    def process_pipelines(self):
        results = ComputePipelines()
        for pg in count(1, self.n_pages):
            print(f"Processing pipelines pages {pg}..{pg + self.n_pages - 1}",
                  file=sys.stderr)
            results_pg = self.process_pages(ComputePipelines, pg,
                                            self.process_pipelines_in_page)
            results += results_pg
            if results_pg.is_empty():
                break
        return results


def generate_report(period, data):
    def md_link(text, link):
        max_len = 40
        if len(text) > max_len:
            text = text[:max_len] + "..."
        return f"[{text}]({link})"

    def format_timedelta(seconds):
        # remove seconds
        return ':'.join(str(timedelta(seconds=seconds)).split(':')[:2])

    # calculate number of pipeline failures from jobs
    n_pipe_failures = len(data.computed_jobs.pipelines)
    # Total calculated from the pipeline
    n_pipe_total = data.n_failed + data.n_success
    pipe_failing_rate = (100 * n_pipe_failures /
                         n_pipe_total) if n_pipe_total else 0

    n_jobs_failures = data.computed_jobs.n_failed
    n_jobs_total = n_jobs_failures + data.computed_jobs.n_success
    jobs_failing_rate = (100 * n_jobs_failures /
                         n_jobs_total) if n_jobs_total else 0

    # Top 10 failing jobs for format [(name, n_failures), (),]
    tabulated_top5_jobs_header = ["job name", "number of failures"]
    tabulated_top5_jobs = sorted(data.computed_jobs.names.items(),
                                 key=lambda kv: kv[1], reverse=True)[:10]
    tabulated_top5_jobs_str = tabulate(tabulated_top5_jobs,
                                       tabulated_top5_jobs_header,
                                       tablefmt="pipe")

    # categories of errors
    tabulated_categories_header = ["error category", "number of occurrences"]
    categories_counter = (
                    data.computed_jobs.compute_extra_info.categories_counter)
    tabulated_categories = sorted(categories_counter.items(),
                                  key=lambda kv: kv[1], reverse=True)[:10]
    tabulated_categories_str = tabulate(tabulated_categories,
                                        tabulated_categories_header,
                                        tablefmt="pipe")

    tabulated_failed_jobs = []
    tabulated_failed_jobs_header = ["job", "pipeline", "stage",
                                    "error category"]
    # Limited to 100 jobs
    mr_names = data.compute_extra_info.mr_info
    error_categories = data.computed_jobs.compute_extra_info.error_categories
    tabulated_failed_jobs.extend(
        [md_link(job.name, job.web_url),
         md_link(mr_names[job.pipeline["id"]], job.pipeline["web_url"]),
         job.stage, error_categories.get(job.id, '')]
        for job in data.computed_jobs.failed_jobs[:100]
    )
    tabulated_failed_jobs_str = tabulate(tabulated_failed_jobs,
                                         tabulated_failed_jobs_header,
                                         tablefmt="pipe")

    if period[0] == period[1]:
        dt = datetime.strptime(period[0], '%Y-%m-%d')
        report_period = f"Daily report for {dt.strftime('%a %b %d %Y')}"
    else:
        report_period = f"{period[0]} - {period[1]}"

    pipelines_more1h = data.compute_extra_info.pipelines_more1h
    n_pipelines_more1h = len(pipelines_more1h)
    pipelines_more1h_rate = (100 * n_pipelines_more1h /
                             n_pipe_total) if n_pipe_total else 0

    pipelines_more30min = data.compute_extra_info.pipelines_more30min
    n_pipelines_more30min = len(pipelines_more30min)
    pipelines_more30min_rate = (100 * n_pipelines_more30min /
                                n_pipe_total) if n_pipe_total else 0

    tabulated_pipelines_more30min_header = ["pipeline", "queued duration",
                                            "exec duration", "total duration"]
    sorted_pipelines_more30min = sorted(
                            pipelines_more30min,
                            key=lambda kv: kv.queued_duration + kv.duration,
                            reverse=True)[:10]
    tabulated_pipelines_more30min = (
        [md_link(mr_names[pipeline.id], pipeline.web_url),
         format_timedelta(pipeline.queued_duration),
         format_timedelta(pipeline.duration),
         format_timedelta(pipeline.queued_duration + pipeline.duration)]
        for pipeline in sorted_pipelines_more30min
        )
    tabulated_pipelines_more30min_str = tabulate(
                                        tabulated_pipelines_more30min,
                                        tabulated_pipelines_more30min_header,
                                        tablefmt="pipe")

    stages_more30min = data.computed_jobs.stages_more30min
    n_jobs_per_stage = data.computed_jobs.n_jobs_per_stage
    n_stages_more30min = len(stages_more30min.keys())
    n_stages_total = len(n_jobs_per_stage.keys())
    stages_more30min_rate = (100 * n_stages_more30min /
                             n_stages_total) if n_stages_total else 0

    tabulated_stages_more30min_header = ["stage", "longest job", "pipeline",
                                         "queued duration", "exec duration",
                                         "total duration"]
    sorted_stages_more30min = sorted(
                stages_more30min.items(),
                key=lambda kv: kv[1].queued_duration + (kv[1].duration or 0),
                reverse=True)[:10]
    tabulated_stages_more30min = (
        [job.stage,
         md_link(job.name, job.web_url),
         md_link(mr_names[job.pipeline["id"]], job.pipeline["web_url"]),
         format_timedelta(job.queued_duration),
         format_timedelta(job.duration or 0),
         format_timedelta(job.queued_duration + (job.duration or 0))]
        for _, job in sorted_stages_more30min
        )
    tabulated_stages_more30min_str = tabulate(
                                        tabulated_stages_more30min,
                                        tabulated_stages_more30min_header,
                                        tablefmt="pipe")

    return f"""
## SUMMARY:

PERIOD: {report_period}

FAILED MERGE PIPELINES: {n_pipe_failures}/{n_pipe_total} - {pipe_failing_rate:.2f}%

FAILED JOBS: {n_jobs_failures}/{n_jobs_total} - {jobs_failing_rate:.2f}%

PIPELINES TAKING MORE THAN 1H: {n_pipelines_more1h}/{n_pipe_total} - {pipelines_more1h_rate:.2f}%

PIPELINES TAKING MORE THAN 30MIN: {n_pipelines_more30min}/{n_pipe_total} - {pipelines_more30min_rate:.2f}%

STAGES TAKING MORE THAN 30MIN: {n_stages_more30min}/{n_stages_total} - {stages_more30min_rate:.2f}%

## DETAILED:

TOP 10 FAILING JOBS:

{tabulated_top5_jobs_str}

ERRORS PER CATEGORY (TOP 10):

{tabulated_categories_str}

FAILED JOBS (limited to 100 entries):

{tabulated_failed_jobs_str}

PIPELINES TAKING MORE THAN 30MIN (TOP 10):

{tabulated_pipelines_more30min_str}

STAGES TAKING MORE THAN 30min (TOP 10):

{tabulated_stages_more30min_str}

## INFORMATION:
  - All numbers only considers {GITLAB_PROJECT_PATH}.
  - Pipelines with updated_at < {period[0]} and jobs with finished_at < {period[0]} were ignored.
  - FAILED MERGE PIPELINES:
      - Calculated from finished pipelines triggered by Marge in a merge requests.
      - Counted when a pipeline contains a failed job, even if it got retried and the pipeline passed (status success).
  - FAILED JOBS: Consider jobs from the same pipelines from FAILED MERGE PIPELINES.
  - PIPELINES TAKING MORE THAN 30MIN also counts PIPELINES TAKING MORE THAN 1H.
  - STAGES TAKING MORE THAN 30MIN: same stages from different pipelines are counted as different stages.

"""  # noqa: E501


def gl_issue_create_report(project, report):
    issue = project.issues.create({'title': 'Mesa CI Daily Report',
                                  'description': report})
    issue.labels = ['CI daily']
    issue.save()
    return issue


def gl_issue_get_report(project, created_after):
    list_args = {
        "created_after": created_after + "T00:00:00.000Z",
        "labels": ['CI daily'],
        "sort": "desc",
        "state": "opened"
    }
    issues = project.issues.list(**list_args)
    return issues[0] if issues else None


def gl_issue_post_comment(issue, comment):
    note = issue.notes.create({'body': comment})
    note.save()
    return note


def gl_send_report(project, report, new_report_date):
    issue = gl_issue_get_report(project, new_report_date)
    if issue:
        gl_issue_post_comment(issue, report)
    else:
        issue = gl_issue_create_report(project, report)
    print(f"Report posted on {issue.web_url}")


def send_email(email_to, report):
    if not email_to:
        return
    # Create the container (outer) email message.
    msg = MIMEText(report)
    msg['Subject'] = 'Mesa CI Daily Report'
    msg['From'] = SMTP_EMAIL
    msg['To'] = email_to

    print(f"Sending report by email from {msg['From']} to {msg['To']}",
          file=sys.stderr)

    s = smtplib.SMTP(SMTP_SERVER, SMTP_SERVER_PORT)
    s.starttls()
    s.login(SMTP_USER, SMTP_PASSWD)
    s.sendmail(SMTP_EMAIL, email_to.split(","), msg.as_string())
    s.quit()


def main(period, email_to, gitlab_post, traces_config):
    first_month_day = period[1][:-2] + '01'
    gl = gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_TOKEN)
    gl.auth()
    project = gl.projects.get(GITLAB_PROJECT_PATH)
    compute = CiProcessor(project, period, traces_config)
    data = compute.process_pipelines()
    report = generate_report(period, data)
    print(report)
    send_email(email_to, report)
    if gitlab_post:
        gl_send_report(project, report, first_month_day)


if __name__ == "__main__":
    yesterday = datetime.strftime(datetime.utcnow() - timedelta(1), '%Y-%m-%d')
    parser = argparse.ArgumentParser()
    parser.add_argument("-df", "--datefrom", help="Starting date XXXX-XX-XX",
                        default=yesterday)
    parser.add_argument("-du", "--dateuntil", help="End date XXXX-XX-XX",
                        default=yesterday)
    parser.add_argument("-et", "--emailto", default="",
                        help="Comma separated emails to send the report to")
    parser.add_argument("-gp", "--gitlab_post", action="store_true",
                        help="Post report to gitlab issue with label 'CI daily'. Create one if no such issue exist for [--dateuntil] month")  # noqa: E501
    parser.add_argument("-cf", "--config",
                        default="mesa-ci-status/traces_config.yaml",
                        help="Yaml configuration file (where errors categories are described)")  # noqa: E501
    args = parser.parse_args()
    with open(args.config) as f:
        traces_config = yaml.load(f, Loader=yaml.FullLoader)
    main((args.datefrom, args.dateuntil), args.emailto, args.gitlab_post,
         traces_config)
